FROM node:18-alpine as build

ARG env
ENV env $env

WORKDIR /usr/src/app

COPY ./package.json ./
COPY ./yarn.lock ./
COPY ./tsconfig.json ./

RUN if [ "$env" = "dev" ]; then yarn install; else yarn install --production; fi;

COPY ./src ./src

RUN if [ "$env" = "dev" ]; then mkdir dist; else yarn build; fi;

FROM node:18-alpine

WORKDIR /usr/src/app

COPY ./package.json ./
COPY ./yarn.lock ./
COPY ./tsconfig.json ./

COPY --from=build /usr/src/app/node_modules ./node_modules
COPY --from=build /usr/src/app/dist ./dist

CMD if [ "$env" = "dev" ]; then npx nodemon src/server.ts; else yarn start; fi;
