import { Router } from "express";
import bodyParser from "body-parser";
import { Static, Type } from "@sinclair/typebox";
import verifyUser from "../lib/auth/verify";
import { puzzlesCl, usersCl } from "../lib/db";
import User from "../models/user";
import { mg } from "../lib/mailgun";
import humanizeDuration from "humanize-duration";

const router = Router();

const schema = Type.Object({
    id: Type.Integer({ minimum: 1 }),
    time: Type.Integer({ minimum: 1 }),
    unsolved: Type.Integer({ minimum: 0 }),
});

router.post(
    "/api/result",
    bodyParser.json(),
    async (
        req: { body: Static<typeof schema>; headers: { authorization?: string } },
        res,
    ) => {
        const { id, time, unsolved } = req.body;

        const user = verifyUser(req.headers.authorization);

        if (!user) return res.status(401).send({ error: "Unauthorized." });

        if (!(await puzzlesCl.findOne({ id })))
            return res.status(404).send({ error: "Puzzle not found." });

        if (
            (((await usersCl.findOne({ id: user.id })) as User)?.solved || [])?.includes(
                id,
            )
        )
            return res
                .status(400)
                .send({ error: "Seems like you have already solved this puzzle." });

        await usersCl.updateOne(
            { id: user.id },
            {
                $push: { results: { id, time, unsolved }, solved: id },
            },
        );

        const userData = (await usersCl.findOne({ id: user.id })) as User;

        const best = userData.results.reduce((acc, curr) =>
            acc.unsolved < curr.unsolved ||
            (acc.unsolved === curr.unsolved && acc.time < curr.time)
                ? acc
                : curr,
        );

        await usersCl.findOneAndUpdate({ id: user.id }, [{ $set: { best } }]);

        mg.messages()
            .send({
                from: "QES English Society <qes-english-society@crossword-puzzle.wcyat.me>",
                to: `${userData.email}`,
                subject: "QES English Society - Thank you for playing crossword puzzle!",
                html: `<h1>Thank you for playing crossword puzzle created by QES English Society!</h1>

                <p>Your best result is: ${humanizeDuration(best.time)}, with ${
                    best.unsolved
                } unsolved.</p>
                
                <p>Click <a href="https://${
                    process.env.domain || "crossword-puzzle.wcyat.me"
                }/rank">here</a> to see the rankings.</p>
                
                <p>You are currently ranking #${
                    (await usersCl.countDocuments({
                        $or: [
                            { "best.unsolved": { $lt: best.unsolved } },
                            {
                                $and: [
                                    { "best.unsolved": best.unsolved },
                                    { "best.time": { $lt: best.time } },
                                ],
                            },
                        ],
                    })) + 1
                } out of ${await usersCl.countDocuments({
                    best: { $exists: true },
                })} players.</p>`,
            })
            .then(console.log)
            .catch(console.log);

        res.send(best);
    },
);

export default router;
