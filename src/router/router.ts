import { Router } from "express";
import users from "./users";
import result from "./result";
import rank from "./rank";
import puzzle from "./puzzle";

const router = Router();

router.use(users);
router.use(result);
router.use(puzzle);
router.use(rank);

export default router;
