import { Router } from "express";
import { Static, Type } from "@sinclair/typebox";
import { ajv } from "../../lib/ajv";
import bodyParser from "body-parser";
import { mg } from "../../lib/mailgun";
import { usersCl, verificationCl } from "../../lib/db";
import { generate } from "wcyat-rg";

const router = Router();

const schema = Type.Object({
    email: Type.RegEx(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@cloud.qes.edu.hk$/),
});

router.post(
    "/api/users/forgot",
    bodyParser.json(),
    async (req: { body: Static<typeof schema> }, res) => {
        if (!ajv.validate(schema, req.body))
            return res.status(400).send({ error: "Bad request." });

        const { email } = req.body;

        if (!(await usersCl.findOne({ email })))
            return res.status(404).send({ error: "User not found." });

        if (await verificationCl.findOne({ email, type: "reset" }))
            return res
                .status(429)
                .send({ error: "You have already requested reset password." });

        const token = generate({
            include: { numbers: true, upper: true, lower: true, special: false },
            digits: 20,
        });

        await verificationCl.insertOne({
            type: "reset",
            email,
            token,
            createdAt: new Date(),
        });

        mg.messages()
            .send({
                from: "QES English Society <qes-english-society@crossword-puzzle.wcyat.me>",
                to: `${email}`,
                subject: "Reset Password - QES English Society",
                html: `<h1>Reset Password</h1>
                       <p>Click 
                       <a href="https://${
                           process.env.domain || "crossword-puzzle.wcyat.me"
                       }/users/reset?email=${email}&token=${token}">here</a> to reset your password.</p>`,
            })
            .then(() => {
                res.status(200).send({ success: "Email sent." });
            })
            .catch(() => {
                res.status(500).send({ error: "Internal server error." });
            });
    },
);

export default router;
