import { Router } from "express";
import bodyParser from "body-parser";
import { Static, Type } from "@sinclair/typebox";
import { ajv } from "../../lib/ajv";
import { usersCl } from "../../lib/db";
import bcrypt from "bcrypt";
import { createToken } from "../../lib/auth/createtoken";
import User from "../../models/user";

const router = Router();

const schema = Type.Object({
    email: Type.RegEx(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@cloud.qes.edu.hk$/),
    password: Type.String(),
});

router.post(
    "/api/users/signin",
    bodyParser.json(),
    async (
        req: {
            body: Static<typeof schema>;
        },
        res,
    ) => {
        const { email, password } = req.body;

        if (!ajv.validate(schema, req.body))
            return res.status(400).json({ error: "Bad request." });

        const userData = (await usersCl.findOne({ email })) as User;

        if (!userData)
            return res
                .status(400)
                .json({ error: "User not found. Please register first." });

        if (!bcrypt.compareSync(password, userData.password))
            return res.status(401).json({ error: "Password incorrect." });

        res.json({
            token: createToken(userData.id, userData.email),
            success: true,
        });
    },
);

export default router;
