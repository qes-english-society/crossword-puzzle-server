import { Router } from "express";
import bodyParser from "body-parser";
import { Static, Type } from "@sinclair/typebox";
import { ajv } from "../../lib/ajv";
import { usersCl, verificationCl } from "../../lib/db";
import bcrypt from "bcrypt";
import { createToken } from "../../lib/auth/createtoken";

const router = Router();

const schema = Type.Object({
    email: Type.RegEx(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@cloud.qes.edu.hk$/),
    password: Type.String(),
    token: Type.String(),
});

router.post(
    "/api/users/reset",
    bodyParser.json(),
    async (req: { body: Static<typeof schema> }, res) => {
        if (!ajv.validate(schema, req.body))
            return res.status(400).json({
                error: "Bad request.",
            });

        const { email, token, password } = req.body;

        if (
            !(await verificationCl.findOne({
                email,
                token,
            }))
        )
            return res.status(400).json({
                error: "Seems like you have not requested password reset.",
            });

        await usersCl.updateOne(
            { email },
            { $set: { password: bcrypt.hashSync(password, 10) } },
        );
        await verificationCl.deleteOne({ email });

        const userData = await usersCl.findOne({ email });

        res.send({ token: createToken(userData.id, userData.email), success: true });
    },
);

export default router;
