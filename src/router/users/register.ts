import { Router } from "express";
import { Type, Static } from "@sinclair/typebox";
import bodyParser from "body-parser";
import { ajv } from "../../lib/ajv";
import { usersCl } from "../../lib/db";
import bcrypt from "bcrypt";
import User from "../../models/user";
import { createToken } from "../../lib/auth/createtoken";

const router = Router();

const schema = Type.Object(
    {
        class_name: Type.RegEx(/^[1-6][a-dA-D]$/),
        class_number: Type.Integer({ maximum: 36, minimum: 1 }),
        email: Type.RegEx(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@cloud.qes.edu.hk$/),
        password: Type.String(),
    },
    { additionalProperties: false },
);

router.post(
    "/api/users/register",
    bodyParser.json(),
    async (
        req: {
            body: Static<typeof schema>;
        },
        res,
    ) => {
        const { email, password, class_number, class_name } = req.body;

        if (!ajv.validate(schema, req.body))
            return res.status(400).json({ error: "Bad request." });

        if (
            await usersCl.findOne({
                $or: [{ email }, { $and: [{ class_name, class_number }] }],
            })
        )
            return res
                .status(400)
                .json({ error: "You have already registered. Please sign in." });

        const userId =
            ((await usersCl.find().sort({ id: -1 }).limit(1).toArray()) as User[])[0]
                ?.id + 1 || 1;

        await usersCl.insertOne({
            id: userId,
            class_name,
            class_number,
            email,
            password: bcrypt.hashSync(password, 10),
        });

        res.send({ token: createToken(userId, email), success: true });
    },
);

export default router;
