import { Router } from "express";
import puzzle from "./puzzle/puzzle";
import puzzles from "./puzzle/puzzles";

const router = Router();

router.use(puzzle);
router.use(puzzles);

export default router;
