import { Router } from "express";
import verifyUser from "../../lib/auth/verify";
import { puzzlesCl, usersCl } from "../../lib/db";
import User from "../../models/user";

const router = Router();

router.get(
    "/api/puzzle",
    async (
        req: {
            headers: {
                authorization?: string;
            };
        },
        res,
    ) => {
        const user = verifyUser(req.headers.authorization);

        if (!user) return res.status(401).send({ error: "Unauthorized" });

        const userData = (await usersCl.findOne({ id: user.id })) as User;

        const puzzle = (
            await puzzlesCl
                .aggregate([
                    { $match: { id: { $nin: userData.solved || [] } } },
                    { $sample: { size: 1 } },
                ])
                .toArray()
        )?.[0];

        res.send(puzzle);
    },
);

export default router;
