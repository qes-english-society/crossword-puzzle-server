import { Router } from "express";
import { puzzlesCl } from "../../lib/db";

const router = Router();

router.get("/api/puzzles", async (req, res) => {
    const puzzles = await puzzlesCl.find().project({ _id: 0, puzzle: 0 }).toArray();

    res.send({ puzzles, count: puzzles.length });
});

export default router;
