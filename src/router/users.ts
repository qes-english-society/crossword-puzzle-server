import { Router } from "express";
import register from "./users/register";
import signin from "./users/signin";
import forgot from "./users/forgot";
import reset from "./users/reset";

const router = Router();

router.use(register);
router.use(signin);
router.use(forgot);
router.use(reset);

export default router;
