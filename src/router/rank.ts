import { Router } from "express";
import { puzzlesCl, usersCl } from "../lib/db";
import { ajv } from "../lib/ajv";
import { Type } from "@sinclair/typebox";

const router = Router();

router.get("/api/rank", async (req, res) => {
    const puzzle = Number(req.query.puzzle);

    if (puzzle) {
        if (
            !ajv.validate(
                Type.Integer({ minimum: 1, maximum: await puzzlesCl.countDocuments() }),
                puzzle,
            )
        )
            return res.status(400).json({ error: "Bad request." });

        const ranks = await usersCl
            .aggregate([
                { $match: { results: { $elemMatch: { id: puzzle } } } },
                { $unwind: "$results" },
                { $match: { "results.id": puzzle } },
                {
                    $project: {
                        _id: 0,
                        class_name: 1,
                        class_number: 1,
                        email: 1,
                        result: "$results",
                    },
                },
                { $sort: { "result.unsolved": 1, "result.time": 1 } },
            ])
            .toArray();

        return res.send(ranks);
    }

    res.send(
        await usersCl
            .find({ best: { $exists: true } })
            .project({ email: 1, result: "$best", class_name: 1, class_number: 1 })
            .sort({ "best.unsolved": 1, "best.time": 1 })
            .toArray(),
    );
});

export default router;
