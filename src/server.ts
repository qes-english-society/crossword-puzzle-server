import cors from "cors";
import express from "express";
import morgan from "morgan";
import router from "./router/router";
import dotenv from "dotenv";
import { client } from "./lib/db";

dotenv.config();

const app = express();

app.use(morgan("dev"));

app.use(cors());

app.use(router);

app.use((req, res) => {
    res.status(404).json({
        error: "Route not found.",
    });
});

client.connect().then(() => {
    app.listen(process.env.port || 3000, () => {
        console.log(`Server is running on port ${process.env.port || 3000}`);
    });
});
