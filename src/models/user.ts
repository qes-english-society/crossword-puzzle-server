import { ObjectId } from "mongodb";
import { result } from "../types/user";

export default class User {
    constructor(
        public id: number,
        public email: string,
        public password: string,
        public class_name: string,
        public class_number: string,
        public best?: result,
        public results?: result[],
        public solved?: number[],
        public _id?: ObjectId,
    ) {}
}
