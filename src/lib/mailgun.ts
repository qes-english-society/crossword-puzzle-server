import mailgun from "mailgun-js";

export const MAILGUN_DOMAIN =
    process.env.MAILGUN_DOMAIN || process.env.domain || "crossword-puzzle.wcyat.me";

export const mg = mailgun({
    apiKey: process.env.MAILGUN_API_KEY,
    domain: MAILGUN_DOMAIN,
});
