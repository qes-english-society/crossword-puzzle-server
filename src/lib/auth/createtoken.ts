import jwt from "jsonwebtoken";
import { jwtTokenType } from "../../types/jwt";

export function createToken(id: number, email: string) {
    const jsonData: jwtTokenType = {
        id,
        email,
        iss: process.env.domain || "",
        aud: process.env.domain || "",
    };
    return jwt.sign(jsonData, process.env.jwtKey || "", {
        algorithm: "HS256",
        expiresIn: "7d",
    });
}
