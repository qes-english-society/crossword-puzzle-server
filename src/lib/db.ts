import { MongoClient } from "mongodb";

const MONGODB_URI = process.env.MONGODB_URI || "mongodb://localhost";

export const client = new MongoClient(MONGODB_URI);

export const db = client.db("crossword");

export const usersCl = db.collection("users");

export const puzzlesCl = db.collection("puzzles");

export const verificationCl = db.collection("verification");
