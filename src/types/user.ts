export type result = { time: number; unsolved: number; id: number };

export type userType = {
    id: number;
    email: string;
    password: string;
    best?: result;
    results?: result[];
    solved?: number[];
};
